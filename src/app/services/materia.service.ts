import { Injectable } from '@angular/core';
import { Materia } from '../models/materia';
import { Tema } from '../models/tema';

@Injectable()
export class MateriaService {

  materias: Materia[] = [];

  constructor() {
    let m1 = new Materia('Redes', 7, 'Ingeniería de sistemas');
    m1.addTema(new Tema('Tema x'));

    let m2 = new Materia('Compugrafica', 7, 'Ingeniería multimedia');
    m2.addTema(new Tema('Tema x'));

    this.materias.push(m1, m2);
   }

  getMaterias() {

    return this.materias;
  }
}
