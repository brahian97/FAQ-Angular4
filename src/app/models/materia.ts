import { Tema } from './tema';

export class Materia {

    nombre: string;
    semestre: number;
    carrera: string;
    temas: Tema[] = [];

    constructor(nombre: string, semestre: number, carrera: string) {
        this.nombre = nombre;
        this.semestre = semestre;
        this.carrera = carrera;
    }

    addTema(tema: Tema) {
        this.temas.push(tema);
    }
}
