import { Component, OnInit } from '@angular/core';
import { Materia } from '../models/materia';
import { MateriaService } from '../services/materia.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  materias: Materia[];

  constructor(private ms: MateriaService) { // Con esto creo el servicio dentro del componente

   }

  ngOnInit() {
    this.materias = this.ms.getMaterias();
  }

}
