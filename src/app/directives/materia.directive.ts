import { Directive, ElementRef, Renderer, HostListener, Input } from '@angular/core';
// ElementRef, Sirve para saber a que elemento estoy haciendo referencia en el DOM
// Renderer, sirve para aplicar y mostrar los cambios hechos sobre el elemento.
// HostListener, ejecua un metodo para un evento dado.

@Directive({
  selector: '[appMateria]'
})
export class MateriaDirective {

  constructor(private el: ElementRef, private renderer: Renderer) {}

  @Input() color: string;

  @HostListener('mouseenter') onmouseenter() {
    this.Resaltar(this.color);
  }

  @HostListener('mouseleave') onmouseleave() {
    this.Resaltar('blue');
  }

  private Resaltar(color: string) {
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', color);
    // el.nativeElement, sirve para acceder a el elemento que se quiere modificar
    // El segundo parametro sirve para determinar la propiedad a cambiar
    // El terce parametro es el valor de la propiedad
  }

}
