import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MateriaService } from './services/materia.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { OtroComponent } from './otro/otro.component';
import { MateriaDirective } from './directives/materia.directive';

const rutasApp: Routes = [
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'otro', component: OtroComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    OtroComponent,
    MateriaDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      rutasApp,
      { enableTracing: true } // <-- debugging  purposes only
    )
  ],
  providers: [
    MateriaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
